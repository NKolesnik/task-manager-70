package ru.t1consulting.nkolesnik.tm.api.endpoint;

import ru.t1consulting.nkolesnik.tm.dto.request.AbstractRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {
    RS execute(RQ request);
}
