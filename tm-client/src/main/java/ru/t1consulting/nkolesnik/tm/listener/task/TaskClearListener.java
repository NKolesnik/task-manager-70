package ru.t1consulting.nkolesnik.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskClearRequest;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear task list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[TASKS CLEAR]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

}
