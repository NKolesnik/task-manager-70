package ru.t1consulting.nkolesnik.tm.exception.project;

import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class ProjectNullException extends AbstractException {

    public ProjectNullException() {
        super("Error! Project is null...");
    }

}
