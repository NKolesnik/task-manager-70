package ru.t1consulting.nkolesnik.tm.exception.user;

public final class UserEmptyIdException extends AbstractUserException {

    public UserEmptyIdException() {
        super("Error! User id is empty...");
    }

}