package ru.t1consulting.nkolesnik.tm.exception.user;

public final class UserEmptyLoginException extends AbstractUserException {

    public UserEmptyLoginException() {
        super("Error! User login is empty...");
    }

}