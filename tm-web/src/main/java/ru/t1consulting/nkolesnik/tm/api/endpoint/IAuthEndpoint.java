package ru.t1consulting.nkolesnik.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1consulting.nkolesnik.tm.model.dto.Result;
import ru.t1consulting.nkolesnik.tm.model.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    @PostMapping(value = "/login")
    Result login(
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password

    );

    @NotNull
    @WebMethod
    @GetMapping(value = "/profile")
    UserDto profile();

    @NotNull
    @WebMethod
    @PostMapping(value = "/logout")
    Result logout();

}
