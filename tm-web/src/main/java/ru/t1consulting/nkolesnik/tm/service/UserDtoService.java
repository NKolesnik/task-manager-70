package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.enumerated.RoleType;
import ru.t1consulting.nkolesnik.tm.exception.user.*;
import ru.t1consulting.nkolesnik.tm.model.dto.RoleDto;
import ru.t1consulting.nkolesnik.tm.model.dto.UserDto;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Optional;

@Service
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USUAL);
        initUser("user", "user", RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserEmptyLoginException();
        if (password == null || password.isEmpty()) throw new UserEmptyPasswordException();
        if (roleType == null) throw new UserEmptyRoleException();
        @Nullable final UserDto user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Override
    @Modifying
    @Transactional
    public UserDto createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserEmptyLoginException();
        if (password == null || password.isEmpty()) throw new UserEmptyPasswordException();
        if (roleType == null) throw new UserEmptyRoleException();
        @NotNull final UserDto user = new UserDto(login, passwordEncoder.encode(password));
        @NotNull final RoleDto role = new RoleDto(user, roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserEmptyLoginException();
        @Nullable final UserDto user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void delete(@Nullable final UserDto user) {
        Optional.ofNullable(user).orElseThrow(UserNullException::new);
        repository.delete(user);
    }

}
