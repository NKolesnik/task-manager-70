package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IDtoService;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.NameComparator;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDto;

import java.util.Comparator;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDto> implements IDtoService<M> {

    protected String getSortColumnName(@Nullable Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == DateBeginComparator.INSTANCE) return "dateBegin";
        else return "status";
    }

}
