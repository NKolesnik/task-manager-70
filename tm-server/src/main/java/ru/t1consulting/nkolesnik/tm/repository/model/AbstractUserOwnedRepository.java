package ru.t1consulting.nkolesnik.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

@Repository
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> {

}
