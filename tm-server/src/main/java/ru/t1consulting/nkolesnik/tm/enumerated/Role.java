package ru.t1consulting.nkolesnik.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return Role.USUAL;
        for (final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        return Role.USUAL;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}